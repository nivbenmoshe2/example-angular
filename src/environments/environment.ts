// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBAvjKedh5ReZ048gsPHh74BHLR7rePj-U",
    authDomain: "example-nivbn.firebaseapp.com",
    projectId: "example-nivbn",
    storageBucket: "example-nivbn.appspot.com",
    messagingSenderId: "316483135073",
    appId: "1:316483135073:web:972e0d6a4a0a41ed226c20"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
