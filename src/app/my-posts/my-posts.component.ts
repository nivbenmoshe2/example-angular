import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { PostsService } from '../posts.service';

@Component({
  selector: 'my-posts',
  templateUrl: './my-posts.component.html',
  styleUrls: ['./my-posts.component.css']
})
export class MyPostsComponent implements OnInit {

  posts$:Observable<any>;
  userId:string;
  public Like : number=0

  addLike(id:string,Likes:number){
    this.Like = Likes + 1 ; 
    this.postsServie.updateLikes(this.userId,id,this.Like)
 }

 deletePost(id){
  this.postsServie.deletePost(this.userId,id)
  console.log(id);
 }

  constructor(private postsServie:PostsService, public authService:AuthService) { }

  ngOnInit(): void {

    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.posts$ = this.postsServie.getCollectionPost(this.userId);
       }
    )
  }

}
