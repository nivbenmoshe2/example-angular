import { LoanService } from './../loan.service';
import { Client } from './../interfaces/client';
import { ClientsService } from './../clients.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  clients:Client[];
  clients$;
  editstate = [];
  addClientFormOpen = false;
  userId:string;
  panelOpenState = false;
  rowToDelete:number = -1; 


  moveToDeleteState(i){
    this.rowToDelete = i; 
  }


  update(client:Client){
    this.clientsService.updateClient(this.userId, client.id, client.name, client.years, client.salary);
  }

  updateResult(i){
    this.clients[i].saved = true; 
    this.clientsService.updateResult(this.userId,this.clients[i].id,this.clients[i].result);
  }

  add(client:Client){
    this.clientsService.addClient(this.userId, client.name, client.years, client.salary);
  }

  deleteClient(id:string){
    this.clientsService.deleteClient(this.userId,id);
    this.rowToDelete = null;
  }

  predict(i){
    console.log(this.clients[i]);
    this.loanService.predict(this.clients[i].years, this.clients[i].salary).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will pay';
        } else {
          var result = 'Will not pay'
        }
        this.clients[i].result = result;
        console.log(result);
      
      }
    ); 
        
      }
    
  

  constructor(public authService:AuthService, private clientsService:ClientsService, private loanService:LoanService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.clients$ = this.clientsService.getClients(this.userId);
        this.clients$.subscribe(
          docs =>{
            this.clients = [];
            for(let document of docs){
              const client:Client = document.payload.doc.data();
              if(client.result){
                client.saved = true; 
              }
              client.id = document.payload.doc.id;
              this.clients.push(client);
            }
          }
        )
      }
    )
  }
}
