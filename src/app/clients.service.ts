import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  clientCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');


  public deleteClient(userId:string , id:string){
    this.db.doc(`users/${userId}/clients/${id}`).delete();
  }

  public updateClient(userId:string, id:string, name:string, years:number, salary:number){
    this.db.doc(`users/${userId}/clients/${id}`).update(
      {
        name:name,
        years:years,
        salary:salary,
        result:null
      }
    )
  }

  updateResult(userId:string, id:string,result:string){
    this.db.doc(`users/${userId}/clients/${id}`).update(
      {
        result:result
      })
    }


  addClient(userId:string, name:string, years:number, salary:number){
    const client = {name:name, years:years, salary:salary};
    this.userCollection.doc(userId).collection('clients').add(client)
  }

  getClients(userId):Observable<any[]>{
    this.clientCollection = this.db.collection(`users/${userId}/clients`);
    return this.clientCollection.snapshotChanges()
  }
/*
  public getClients(userId):Observable<any[]>{
    this.clientCollection = this.db.collection(`users/${userId}/clients`);
    return this.clientCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }
*/
  constructor(private db:AngularFirestore) { }
}
