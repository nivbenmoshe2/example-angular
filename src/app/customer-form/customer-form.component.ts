import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  @Input() name: string;
  @Input() years: number;
  @Input() id: string;
  @Input() salary: number;
  @Input() formType: string;
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();
  isError:boolean = false;

  buttonText:String = 'Add Customer'; 

  tellParentToClose(){
    this.closeEdit.emit();
  }

  updateParent(){
    let customer:Customer = {id:this.id,name:this.name, years:this.years,salary:this.salary}; 
    if(this.years < 0 || this.years > 24){
      this.isError = true;
    }
    else{
    this.update.emit(customer);
    if(this.formType == 'Add Customer'){
      this.name = null;
      this.years = null;
      this.salary = null; 
      }
    }
  }

  constructor() { }

  ngOnInit(): void {
    if(this.formType == 'Add Customer'){
      this.buttonText = 'Add';
    }
  }
}
