import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  email:string;

  constructor(public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.email = user.email;
      }
    )

  }

}
