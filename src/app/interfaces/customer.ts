export interface Customer {
    id?:string,
    name:string,
    years:number,
    salary:number,
    result?:string,
    saved?:boolean
}
