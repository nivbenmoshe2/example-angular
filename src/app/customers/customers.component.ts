import { PredictionService } from './../prediction.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  userId;
  customers:Customer[];
  customers$;
  addCustomerFormOpen;
  rowToEdit:number = -1; 
  customerToEdit:Customer = {name:null, years:null, salary:null};

  add(customer:Customer){
    this.customersService.addCustomer(this.userId, customer.name, customer.years, customer.salary)
  }
  
  moveToEditState(index){
    console.log(this.customers[index].name);
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.years = this.customers[index].years;
    this.customerToEdit.salary = this.customers[index].salary;
    this.rowToEdit = index; 
  }

  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.customersService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.years,this.customerToEdit.salary);
    this.rowToEdit = null;
  }

  deleteCustomer(index){
    let id = this.customers[index].id;
    this.customersService.deleteCustomer(this.userId, id);
  }

  updateResult(index){
    this.customers[index].saved = true; 
    this.customersService.updateResult(this.userId,this.customers[index].id,this.customers[index].result);
  }

  predict(index){
    this.customers[index].result = 'Loading..';
    this.predictionService.predict(this.customers[index].years, this.customers[index].salary).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will pay';
        } else {
          var result = 'Will not pay'
        }
        this.customers[index].result = result}
    );  
  }

  displayedColumns: string[] = ['Name', 'Education in years', 'Personal salary','Delete', 'Edit', 'Predict', 'Result'];

  constructor(private customersService:CustomersService, public authService:AuthService, private predictionService:PredictionService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.customers$ = this.customersService.getCollectionCustomers(this.userId);
          this.customers$.subscribe(
            docs => {         
              this.customers = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const customer:Customer = document.payload.doc.data();
                if(customer.result){
                  customer.saved = true; 
                }
                customer.id = document.payload.doc.id;
                   this.customers.push(customer); 
              }  
            }
          )
      })
  }

}
