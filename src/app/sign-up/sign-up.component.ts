import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  email:string;
  password:string;
  errorMessage:string;
  isError:boolean = false;

  onSubmit(){
    this.auth.register(this.email,this.password).then(
      res => {
      console.log('Succesful login;');
      this.router.navigate(['/welcome']);
    }).catch(
      err => {
        console.log(err);
        this.isError = true;
        this.errorMessage = err.message;
      }
    )
  }

  constructor(private auth:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

}
