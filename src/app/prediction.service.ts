import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  url = 'https://owrx4tdule.execute-api.us-east-1.amazonaws.com/beta'; 
  
  predict(years:number, salary:number):Observable<any>{
    let json = {
      "peoples":[ 
        {
          "years": years,
          "salary": salary
        }]
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);     
        let final:string = res.body;
        console.log(final);
        final = final.replace('[','');
        final = final.replace(']','');
        return final; 
      })
    );      
  }

  constructor(private http: HttpClient) { }
}
