import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoanService {

  private url = "https://uz6h9cpfzd.execute-api.us-east-1.amazonaws.com/testNivBenMoshe";


  predict(years:number, salary:number){
    let json = {'peoples':[
      {'years':years, 'salary':salary}
    ]}
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res);
        let final = res.body;
        console.log(final);
        final = final.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;
      })
    )
  }

  constructor(private http:HttpClient) { }
}
