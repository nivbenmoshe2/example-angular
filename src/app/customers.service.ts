import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  customersCollection:AngularFirestoreCollection;

  addCustomer(userId:string, name:string, years:number, salary:number){
    const customer = {name:name, years:years, salary:salary};
    this.userCollection.doc(userId).collection('customers').add(customer)
  }

  public deleteCustomer(userId:string , id:string){
    this.db.doc(`users/${userId}/customers/${id}`).delete();
  }

  public updateCustomer(userId:string, id:string, name:string, years:number, salary:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        salary:salary,
        result:null
      }
    )
  }

  updateResult(userId:string, id:string,result:string){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        result:result
      })
    }

  getCollectionCustomers(userId): Observable<any[]>{
    this.customersCollection = this.db.collection(`users/${userId}/customers`, 
    ref => ref.limit(10))
    return this.customersCollection.snapshotChanges();  
  }

  constructor(private db: AngularFirestore) { }
}
